# Gradle Slacker Plugin

A simple gradle plugin which posts a message to Slack. 

![Example Output](http://i.imgur.com/gBcY6Lx.png)
### Usage
The code for the above example is shown below indicating all of the optional and required fields:

```gradle
slack {
    hookUrl = "WebHook URL" // Required
    botName = "MyBot" // Optional - defaults to "GradleBot"
    channel = "#myChannel" // Required
    message = "My custom Slack message." // Required
    color = "good" // Optional - one of 'good', 'warning', 'danger' or a custom hex value
    fields = [ // optional. Key must be a string and value must be .toString() compliant
            "Custom Field": "Custom Value"
    ]
}
```
Put the above in your `build.gradle` file. To run the task execute `gradlew postToSlack`