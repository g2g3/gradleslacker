package com.g2g3.gradle

import net.gpedro.integrations.slack.SlackApi
import net.gpedro.integrations.slack.SlackAttachment
import net.gpedro.integrations.slack.SlackField
import net.gpedro.integrations.slack.SlackMessage
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Created by valentin.hinov on 13/05/2016.
 */
class SlackerPlugin implements Plugin<Project> {

    @Override
    void apply(Project target) {

        target.extensions.create('slack', SlackerPluginExtension)

        def postToSlackTask = target.tasks.create("postToSlack") << {
            def api = new SlackApi(target.slack.hookUrl)

            def botName = target.slack.botName
            if (botName == null) {
                botName = "GradleBot"
            }

            def message = new SlackMessage(target.slack.channel, botName, "")

            def attachment = new SlackAttachment()
            attachment.setText(target.slack.message)
            attachment.setColor(target.slack.color)
            attachment.setFallback(target.slack.message)

            def fields = (Map<String, Object>) target.slack.fields
            fields.each { title, value ->
                def slackField = new SlackField()
                slackField.setTitle(title)
                slackField.setValue(value.toString())

                def shorten = value.toString().length() < 30
                slackField.setShorten(shorten)

                attachment.addFields(slackField)
            }

            message.addAttachments(attachment)

            api.call(message)
            println "Posted to Slack " + message.toString()
        }

        postToSlackTask.group = "slackplugin"
        postToSlackTask.description = "Posts a customisable message to slack"
    }
}
