package com.g2g3.gradle

/**
 * Created by valentin.hinov on 13/05/2016.
 */
class SlackerPluginExtension {
    String hookUrl
    String botName
    String channel
    String message
    String color

    Map<String, Object> fields = [:]
}
